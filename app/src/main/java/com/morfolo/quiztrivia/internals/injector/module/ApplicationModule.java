package com.morfolo.quiztrivia.internals.injector.module;

import android.app.Application;
import android.content.Context;

import com.morfolo.quiztrivia.internals.injector.scope.ApplicationContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Morfolo on 24/11/2017.
 */

@Module
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    public Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    public Context provideApplicationContext() {
        return mApplication.getApplicationContext();
    }
}

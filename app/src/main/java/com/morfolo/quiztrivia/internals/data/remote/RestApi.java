package com.morfolo.quiztrivia.internals.data.remote;

import com.morfolo.quiztrivia.internals.data.local.Quiz;
import com.morfolo.quiztrivia.internals.data.local.Reset;
import com.morfolo.quiztrivia.internals.data.local.Token;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Morfolo on 24/11/2017.
 */

public interface RestApi {

    @GET("api.php")
    Observable<Quiz> getQuiz(
            @Query("amount") int amount,
            @Query("category") int category,
            @Query("difficulty") String difficulty,
            @Query("type") String type,
            @Query("token") String token);

    @GET("api_token.php")
    Observable<Token> getToken(@Query("command") String command);

    @GET("api_token.php")
    Observable<Reset> getReset(@Query("command") String command, @Query("token") String token);
}

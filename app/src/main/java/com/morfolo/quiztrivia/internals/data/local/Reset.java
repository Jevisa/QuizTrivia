package com.morfolo.quiztrivia.internals.data.local;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class Reset {

    @SerializedName("response_code")
    private int responseCode;
    @SerializedName("token")
    private String token;

    public int getResponseCode() {
        return responseCode;
    }

    public String getToken() {
        return token;
    }
}

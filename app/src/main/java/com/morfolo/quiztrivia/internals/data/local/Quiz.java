package com.morfolo.quiztrivia.internals.data.local;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Morfolo on 24/11/2017.
 */

public class Quiz {

    @SerializedName("response_code")
    private int responseCode;
    @SerializedName("results")
    private ArrayList<Results> results;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public ArrayList<Results> getResults() {
        return results;
    }

    public void setResults(ArrayList<Results> results) {
        this.results = results;
    }

    public static class Results implements Parcelable {

        @SerializedName("category")
        private String category;
        @SerializedName("type")
        private String type;
        @SerializedName("difficulty")
        private String difficulty;
        @SerializedName("question")
        private String question;
        @SerializedName("correct_answer")
        private String correctAnswer;
        @SerializedName("incorrect_answers")
        private ArrayList<String> incorrectAnswers;

        public Results() {
        }

        protected Results(Parcel in) {
            category = in.readString();
            type = in.readString();
            difficulty = in.readString();
            question = in.readString();
            correctAnswer = in.readString();
            incorrectAnswers = in.createStringArrayList();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(category);
            dest.writeString(type);
            dest.writeString(difficulty);
            dest.writeString(question);
            dest.writeString(correctAnswer);
            dest.writeStringList(incorrectAnswers);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @SuppressWarnings("unused")
        public static final Creator<Results> CREATOR = new Creator<Results>() {
            @Override
            public Results createFromParcel(Parcel in) {
                return new Results(in);
            }

            @Override
            public Results[] newArray(int size) {
                return new Results[size];
            }
        };

        public String getCategory() {
            return category;
        }

        public String getType() {
            return type;
        }

        public String getDifficulty() {
            return difficulty;
        }

        public String getQuestion() {
            return question;
        }

        public String getCorrectAnswer() {
            return correctAnswer;
        }

        public ArrayList<String> getIncorrectAnswers() {
            return incorrectAnswers;
        }
    }
}

package com.morfolo.quiztrivia.internals.injector.module;

import android.app.Activity;
import android.content.Context;

import com.morfolo.quiztrivia.internals.injector.scope.ActivityContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Morfolo on 24/11/2017.
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    public Activity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    public Context provideActivityContext() {
        return mActivity;
    }
}

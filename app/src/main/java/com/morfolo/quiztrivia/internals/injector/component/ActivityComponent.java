package com.morfolo.quiztrivia.internals.injector.component;

import android.content.Context;

import com.morfolo.quiztrivia.internals.injector.module.ActivityModule;
import com.morfolo.quiztrivia.internals.injector.scope.ActivityContext;
import com.morfolo.quiztrivia.internals.injector.scope.ActivityScope;
import com.morfolo.quiztrivia.views.activity.category.CategoryActivity;
import com.morfolo.quiztrivia.views.activity.quiz.QuizActivity;

import dagger.Component;

/**
 * Created by Morfolo on 24/11/2017.
 */

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    @ActivityContext
    Context getContext();

    void inject(CategoryActivity activity);

    void inject(QuizActivity activity);
}

package com.morfolo.quiztrivia.internals.injector.component;

import android.app.Application;
import android.content.Context;

import com.morfolo.quiztrivia.internals.data.remote.RestApi;
import com.morfolo.quiztrivia.internals.injector.module.ApplicationModule;
import com.morfolo.quiztrivia.internals.injector.module.NetworkModule;
import com.morfolo.quiztrivia.internals.injector.scope.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Morfolo on 24/11/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context getContext();

    void inject(Application application);

    RestApi getApi();
}

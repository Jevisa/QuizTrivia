package com.morfolo.quiztrivia.views.costum;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.morfolo.quiztrivia.R;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class DialogView {

    private static DialogView sInstance;

    private AlertDialog dialog;

    public static DialogView getInstance() {
        if (sInstance == null) {
            sInstance = new DialogView();
        }
        return sInstance;
    }

    public void show(Context context) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.view_progress_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setView(dialogView)
                .setCancelable(false);
        dialog = builder.create();
        dialog.show();
    }

    public void hide() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}

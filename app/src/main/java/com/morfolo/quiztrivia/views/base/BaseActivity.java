package com.morfolo.quiztrivia.views.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.morfolo.quiztrivia.BaseApp;
import com.morfolo.quiztrivia.internals.injector.component.ActivityComponent;
import com.morfolo.quiztrivia.internals.injector.component.DaggerActivityComponent;
import com.morfolo.quiztrivia.internals.injector.module.ActivityModule;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Morfolo on 25/11/2017.
 */

public abstract class BaseActivity<T extends IView, V extends BasePresenter<T>> extends AppCompatActivity {

    @Inject
    V mPresenter;

    private Unbinder mUnbinder;

    private ActivityComponent mComponent;

    @LayoutRes
    protected abstract int getContentResource();

    protected abstract void init(Bundle state);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResource());
        mUnbinder = ButterKnife.bind(this);

        mComponent = DaggerActivityComponent.builder()
                .applicationComponent(BaseApp.getComponent())
                .activityModule(new ActivityModule(this))
                .build();

        init(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    public V getPresenter() {
        return mPresenter;
    }

    public ActivityComponent getComponent() {
        return mComponent;
    }

}

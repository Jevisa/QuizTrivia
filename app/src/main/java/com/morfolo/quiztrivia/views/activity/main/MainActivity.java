package com.morfolo.quiztrivia.views.activity.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.morfolo.quiztrivia.R;
import com.morfolo.quiztrivia.views.activity.category.CategoryActivity;
import com.morfolo.quiztrivia.views.activity.score.ScoreActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    private Unbinder mUnbinder;

    @OnClick(R.id.buttonQuiz)
    public void quizClick() {
        CategoryActivity.startThisActivity(this);
    }

    @OnClick(R.id.buttonScore)
    public void scoreClick() {
        ScoreActivity.startThisActivity(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUnbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }
}

package com.morfolo.quiztrivia.views.base;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class BasePresenter<T extends IView> implements IPresenter<T> {

    private T mView;

    private CompositeDisposable mDisposable;

    @Override
    public void onAttach(T view) {
        mView = view;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        mView = null;
        mDisposable.clear();
    }

    public T getView() {
        return mView;
    }

    public CompositeDisposable getDisposable() {
        return mDisposable;
    }
}

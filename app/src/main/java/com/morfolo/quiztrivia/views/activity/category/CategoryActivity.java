package com.morfolo.quiztrivia.views.activity.category;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.morfolo.quiztrivia.R;
import com.morfolo.quiztrivia.views.activity.quiz.QuizActivity;
import com.morfolo.quiztrivia.views.adapter.CategoryRecyclerAdapter;
import com.morfolo.quiztrivia.views.adapter.ItemOffsetDecoration;
import com.morfolo.quiztrivia.views.base.BaseActivity;
import com.morfolo.quiztrivia.views.costum.DialogView;

import butterknife.BindView;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class CategoryActivity extends BaseActivity<CategoryContract.View, CategoryPresenter>
        implements CategoryContract.View, CategoryRecyclerAdapter.CategoryListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private String mCategory = "";
    private int mCode = 0;
    private String mdifficulty;

    public static void startThisActivity(Context context) {
        Intent intent = new Intent(context, CategoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected int getContentResource() {
        return R.layout.activity_category;
    }

    @Override
    protected void init(Bundle state) {
        inject();
        onAttachView();

        initRecycler();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }

    @Override
    public void inject() {
        getComponent().inject(this);
    }

    @Override
    public void onAttachView() {
        getPresenter().onAttach(this);
    }

    @Override
    public void onDetachView() {
        getPresenter().onDetach();
    }

    @Override
    public void onStartProgress() {
        DialogView.getInstance().show(this);
    }

    @Override
    public void onStopProgress() {
        DialogView.getInstance().hide();
    }

    @Override
    public void onTokenSuccess() {
        final ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1);
        mAdapter.add(getString(R.string.text_easy));
        mAdapter.add(getString(R.string.text_medium));
        mAdapter.add(getString(R.string.text_hard));

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.dialog_title))
                .setAdapter(mAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mdifficulty = mAdapter.getItem(which);

                        QuizActivity.startThisActivity(CategoryActivity.this, mCode, mCategory, mdifficulty);
                    }
                });
        builder.create().show();
    }

    @Override
    public void onError() {
        Toast.makeText(this, getString(R.string.message_text_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(String category, int code) {
        mCategory = category;
        mCode = code;

        getPresenter().getTokenFromServer();
    }

    private void initRecycler() {
        CategoryRecyclerAdapter mAdapter = new CategoryRecyclerAdapter(this);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.size_8));
        recyclerView.setAdapter(mAdapter);
    }
}

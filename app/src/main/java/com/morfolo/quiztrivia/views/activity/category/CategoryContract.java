package com.morfolo.quiztrivia.views.activity.category;

import com.morfolo.quiztrivia.internals.data.local.Quiz;
import com.morfolo.quiztrivia.views.base.IView;

import java.util.ArrayList;

/**
 * Created by Morfolo on 25/11/2017.
 */

public interface CategoryContract {

    interface View extends IView {

        void onStartProgress();

        void onStopProgress();

        void onTokenSuccess();

        void onError();
    }

    interface Presenter {

        void getTokenFromServer();
    }
}

package com.morfolo.quiztrivia.views.base;

/**
 * Created by Morfolo on 25/11/2017.
 */

public interface IPresenter<T extends IView> {

    void onAttach(T view);

    void onDetach();
}

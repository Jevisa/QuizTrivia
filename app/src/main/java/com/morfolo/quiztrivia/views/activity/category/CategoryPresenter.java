package com.morfolo.quiztrivia.views.activity.category;

import com.morfolo.quiztrivia.internals.data.local.Quiz;
import com.morfolo.quiztrivia.internals.data.local.Token;
import com.morfolo.quiztrivia.internals.data.remote.RestApi;
import com.morfolo.quiztrivia.utils.CacheManager;
import com.morfolo.quiztrivia.utils.IConstans;
import com.morfolo.quiztrivia.views.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class CategoryPresenter extends BasePresenter<CategoryContract.View> implements CategoryContract.Presenter {

    private static final String COMMAND = "request";

    @Inject
    RestApi mApi;

    @Inject
    public CategoryPresenter() {
    }

    @Override
    public void getTokenFromServer() {
        getView().onStartProgress();
        getDisposable().add(
                mApi.getToken(COMMAND)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(new DisposableObserver<Token>() {
                            @Override
                            public void onNext(Token token) {
                                if(token.getResponseCode() == 0){
                                    CacheManager.getInstance().putString(IConstans.Pref.token, token.getToken());

                                    getView().onTokenSuccess();
                                }else {
                                    getView().onError();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                getView().onStopProgress();
                                getView().onError();
                            }

                            @Override
                            public void onComplete() {
                                getView().onStopProgress();
                            }
                        })
        );
    }
}

package com.morfolo.quiztrivia.views.activity.quiz;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.morfolo.quiztrivia.R;
import com.morfolo.quiztrivia.internals.data.local.Quiz;
import com.morfolo.quiztrivia.utils.CacheManager;
import com.morfolo.quiztrivia.utils.Commons;
import com.morfolo.quiztrivia.utils.IConstans;
import com.morfolo.quiztrivia.views.activity.category.CategoryActivity;
import com.morfolo.quiztrivia.views.base.BaseActivity;
import com.morfolo.quiztrivia.views.costum.DialogView;
import com.morfolo.quiztrivia.views.costum.ItemAnswer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class QuizActivity extends BaseActivity<QuizContract.View, QuizPresenter> implements QuizContract.View {

    private static final int AMOUNT = 20;

    @BindView(R.id.textQuestionStep)
    TextView textQuestionStep;
    @BindView(R.id.textAnswer)
    TextView textAnswer;
    @BindView(R.id.containerQuestion)
    ScrollView containerQuestion;
    @BindView(R.id.textQuestion)
    TextView textQuestion;
    @BindView(R.id.choiceA)
    ItemAnswer choiceA;
    @BindView(R.id.choiceB)
    ItemAnswer choiceB;
    @BindView(R.id.choiceC)
    ItemAnswer choiceC;
    @BindView(R.id.choiceD)
    ItemAnswer choiceD;
    @BindView(R.id.buttonNext)
    Button buttonNext;

    private ArrayList<Quiz.Results> mResults;

    private int mCode = 0;
    private String mDifficulty = "";

    private int mQuestionStep = 1;

    private String correntAnswer;
    private String strChoiceA;
    private String strChoiceB;
    private String strChoiceC;
    private String strChoiceD;

    private boolean isFinish = false;
    private int countCorrectAnswer = 0;

    private int positionItem = -1;

    public static void startThisActivity(Context context, int code, String category, String difficulty) {
        Intent intent = new Intent(context, QuizActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IConstans.Extra.code, code);
        intent.putExtra(IConstans.Extra.category, category);
        intent.putExtra(IConstans.Extra.difficulty, difficulty);
        context.startActivity(intent);
    }

    @Override
    protected int getContentResource() {
        return R.layout.activity_quiz;
    }

    @Override
    protected void init(Bundle state) {
        inject();
        onAttachView();

        initLayout();

        choiceA.setItemAnswerListener(new ItemAnswer.ItemAnswerListener() {
            @Override
            public void onClickItem(View view) {
                choiceA.setBackgroundResource(R.drawable.ic_item_selected);
                choiceB.setBackgroundResource(R.drawable.ic_item_normal);
                choiceC.setBackgroundResource(R.drawable.ic_item_normal);
                choiceD.setBackgroundResource(R.drawable.ic_item_normal);

                positionItem = 0;
            }
        });

        choiceB.setItemAnswerListener(new ItemAnswer.ItemAnswerListener() {
            @Override
            public void onClickItem(View view) {
                choiceA.setBackgroundResource(R.drawable.ic_item_normal);
                choiceB.setBackgroundResource(R.drawable.ic_item_selected);
                choiceC.setBackgroundResource(R.drawable.ic_item_normal);
                choiceD.setBackgroundResource(R.drawable.ic_item_normal);

                positionItem = 1;
            }
        });

        choiceC.setItemAnswerListener(new ItemAnswer.ItemAnswerListener() {
            @Override
            public void onClickItem(View view) {
                choiceA.setBackgroundResource(R.drawable.ic_item_normal);
                choiceB.setBackgroundResource(R.drawable.ic_item_normal);
                choiceC.setBackgroundResource(R.drawable.ic_item_selected);
                choiceD.setBackgroundResource(R.drawable.ic_item_normal);

                positionItem = 2;
            }
        });

        choiceD.setItemAnswerListener(new ItemAnswer.ItemAnswerListener() {
            @Override
            public void onClickItem(View view) {
                choiceA.setBackgroundResource(R.drawable.ic_item_normal);
                choiceB.setBackgroundResource(R.drawable.ic_item_normal);
                choiceC.setBackgroundResource(R.drawable.ic_item_normal);
                choiceD.setBackgroundResource(R.drawable.ic_item_selected);

                positionItem = 3;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }

    @Override
    public void inject() {
        getComponent().inject(this);
    }

    @Override
    public void onAttachView() {
        getPresenter().onAttach(this);
    }

    @Override
    public void onDetachView() {
        getPresenter().onDetach();
    }

    @Override
    public void onStartProgress() {
        DialogView.getInstance().show(this);
    }

    @Override
    public void onStopProgress() {
        DialogView.getInstance().hide();
    }

    @Override
    public void onLoadSuccess(ArrayList<Quiz.Results> results) {
        mResults = results;

        initItemAnswer();
    }

    @Override
    public void onResetSuccess() {
        int easy = CacheManager.getInstance().getInt(IConstans.Pref.easy);
        int medium = CacheManager.getInstance().getInt(IConstans.Pref.medium);
        int hard = CacheManager.getInstance().getInt(IConstans.Pref.hard);

        if (mDifficulty.equals(getString(R.string.text_easy))) {
            if (countCorrectAnswer * 5 > easy) {
                CacheManager.getInstance().putInt(IConstans.Pref.easy, countCorrectAnswer * 5);
            }
        } else if (mDifficulty.equals(getString(R.string.text_medium))) {
            if (countCorrectAnswer * 5 > medium) {
                CacheManager.getInstance().putInt(IConstans.Pref.medium, countCorrectAnswer * 5);
            }
        } else {
            if (countCorrectAnswer * 5 > hard) {
                CacheManager.getInstance().putInt(IConstans.Pref.hard, countCorrectAnswer * 5);
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.action_text_score))
                .setCancelable(false)
                .setMessage(String.valueOf(countCorrectAnswer * 5))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isFinish = false;

                        CategoryActivity.startThisActivity(QuizActivity.this);
                    }
                });
        builder.show();
    }

    @Override
    public void onError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.message_text_error))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().getQuestionFromServer(mCode, mDifficulty);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        builder.show();
    }

    private void initLayout() {
        mCode = getIntent().getIntExtra(IConstans.Extra.code, 0);
        String mCategory = getIntent().getStringExtra(IConstans.Extra.category);
        mDifficulty = getIntent().getStringExtra(IConstans.Extra.difficulty);

        if (mCategory != null && mDifficulty != null) {

            ActionBar mActionBar = getSupportActionBar();
            assert mActionBar != null;
            mActionBar.setDisplayShowTitleEnabled(true);
            mActionBar.setTitle(mCategory);

            getPresenter().getQuestionFromServer(mCode, mDifficulty);
        }

        textQuestionStep.setText(String.format("Question No.%s", mQuestionStep));
        textAnswer.setText(String.format("%s Right Answer", countCorrectAnswer));

        final Handler handler = new Handler();
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFinish) {
                    checkAnswer();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getPresenter().resetTokenFromServer();
                        }
                    }, 1000);
                } else {
                    if (mQuestionStep <= AMOUNT) {
                        checkAnswer();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mQuestionStep = mQuestionStep + 1;
                                textQuestionStep.setText(String.format("Question No.%s", mQuestionStep));
                                initItemAnswer();
                                if (mQuestionStep == AMOUNT) {
                                    buttonNext.setText(getString(R.string.button_finish));
                                    isFinish = true;
                                }
                            }
                        }, 1000);

                    }
                }
            }
        });
    }

    private void initItemAnswer() {

        positionItem = -1;

        choiceA.setBackgroundResource(R.drawable.ic_item_normal);
        choiceA.setTextAnswerColor(android.R.color.black);
        choiceA.hideStatus(true);

        choiceB.setBackgroundResource(R.drawable.ic_item_normal);
        choiceB.setTextAnswerColor(android.R.color.black);
        choiceB.hideStatus(true);

        choiceC.setBackgroundResource(R.drawable.ic_item_normal);
        choiceC.setTextAnswerColor(android.R.color.black);
        choiceC.hideStatus(true);

        choiceD.setBackgroundResource(R.drawable.ic_item_normal);
        choiceD.setTextAnswerColor(android.R.color.black);
        choiceD.hideStatus(true);

        if (mResults != null) {
            containerQuestion.setVisibility(View.VISIBLE);
            textQuestion.setText(Commons.toText(mResults.get(mQuestionStep - 1).getQuestion()));

            correntAnswer = Commons.toText(mResults.get(mQuestionStep - 1).getCorrectAnswer());

            Set<String> set = new HashSet<>(mResults.get(mQuestionStep - 1).getIncorrectAnswers().size() + 1);
            set.addAll(mResults.get(mQuestionStep - 1).getIncorrectAnswers());
            set.add(correntAnswer);

            List<String> shuffleList = new ArrayList<>(set.size());
            shuffleList.addAll(set);

            Collections.shuffle(shuffleList);

            strChoiceA = Commons.toText(shuffleList.get(0));
            strChoiceB = Commons.toText(shuffleList.get(1));
            strChoiceC = Commons.toText(shuffleList.get(2));
            strChoiceD = Commons.toText(shuffleList.get(3));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    choiceA.setTextAnswer(strChoiceA);
                    choiceB.setTextAnswer(strChoiceB);
                    choiceC.setTextAnswer(strChoiceC);
                    choiceD.setTextAnswer(strChoiceD);

                    choiceA.setTextChoice("A");
                    choiceB.setTextChoice("B");
                    choiceC.setTextChoice("C");
                    choiceD.setTextChoice("D");

                    if (correntAnswer.equals(strChoiceA)) {
                        choiceA.setTextStatus("Correct Answer");
                        choiceB.setTextStatus("Wrong Answer");
                        choiceC.setTextStatus("Wrong Answer");
                        choiceD.setTextStatus("Wrong Answer");
                    } else if (correntAnswer.equals(strChoiceB)) {
                        choiceA.setTextStatus("Wrong Answer");
                        choiceB.setTextStatus("Correct Answer");
                        choiceC.setTextStatus("Wrong Answer");
                        choiceD.setTextStatus("Wrong Answer");
                    } else if (correntAnswer.equals(strChoiceC)) {
                        choiceA.setTextStatus("Wrong Answer");
                        choiceB.setTextStatus("Wrong Answer");
                        choiceC.setTextStatus("Correct Answer");
                        choiceD.setTextStatus("Wrong Answer");
                    } else if (correntAnswer.equals(strChoiceD)) {
                        choiceA.setTextStatus("Wrong Answer");
                        choiceB.setTextStatus("Wrong Answer");
                        choiceC.setTextStatus("Wrong Answer");
                        choiceD.setTextStatus("Correct Answer");
                    }
                }
            });
        }
    }

    private void checkAnswer() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (positionItem != -1) {
                    switch (positionItem) {
                        case 0:
                            if (strChoiceA.equals(correntAnswer)) {
                                choiceA.setBackgroundResource(R.drawable.ic_item_correct);
                                choiceA.setTextAnswerColor(android.R.color.white);
                                choiceA.hideStatus(false);

                                countCorrectAnswer = countCorrectAnswer + 1;
                                textAnswer.setText(String.format("%s Right Answer", countCorrectAnswer));
                            } else {
                                choiceA.setBackgroundResource(R.drawable.ic_item_incorrect);
                                choiceA.setTextAnswerColor(android.R.color.white);
                                choiceA.hideStatus(false);
                            }
                            break;
                        case 1:
                            if (strChoiceB.equals(correntAnswer)) {
                                choiceB.setBackgroundResource(R.drawable.ic_item_correct);
                                choiceB.setTextAnswerColor(android.R.color.white);
                                choiceB.hideStatus(false);

                                countCorrectAnswer = countCorrectAnswer + 1;
                                textAnswer.setText(String.format("%s Right Answer", countCorrectAnswer));
                            } else {
                                choiceB.setBackgroundResource(R.drawable.ic_item_incorrect);
                                choiceB.setTextAnswerColor(android.R.color.white);
                                choiceB.hideStatus(false);
                            }
                            break;
                        case 2:
                            if (strChoiceC.equals(correntAnswer)) {
                                choiceC.setBackgroundResource(R.drawable.ic_item_correct);
                                choiceC.setTextAnswerColor(android.R.color.white);
                                choiceC.hideStatus(false);

                                countCorrectAnswer = countCorrectAnswer + 1;
                                textAnswer.setText(String.format("%s Right Answer", countCorrectAnswer));
                            } else {
                                choiceC.setBackgroundResource(R.drawable.ic_item_incorrect);
                                choiceC.setTextAnswerColor(android.R.color.white);
                                choiceC.hideStatus(false);
                            }
                            break;
                        case 3:
                            if (strChoiceD.equals(correntAnswer)) {
                                choiceD.setBackgroundResource(R.drawable.ic_item_correct);
                                choiceD.setTextAnswerColor(android.R.color.white);
                                choiceD.hideStatus(false);

                                countCorrectAnswer = countCorrectAnswer + 1;
                                textAnswer.setText(String.format("%s Right Answer", countCorrectAnswer));
                            } else {
                                choiceD.setBackgroundResource(R.drawable.ic_item_incorrect);
                                choiceD.setTextAnswerColor(android.R.color.white);
                                choiceD.hideStatus(false);
                            }
                            break;
                        default:
                            break;
                    }

                    displayCorrectAnswer();
                } else {
                    displayCorrectAnswer();
                }
            }
        });
    }

    private void displayCorrectAnswer() {
        if (strChoiceA.equals(correntAnswer)) {
            choiceA.setBackgroundResource(R.drawable.ic_item_correct);
            choiceA.setTextAnswerColor(android.R.color.white);
            choiceA.hideStatus(false);
        } else if (strChoiceB.equals(correntAnswer)) {
            choiceB.setBackgroundResource(R.drawable.ic_item_correct);
            choiceB.setTextAnswerColor(android.R.color.white);
            choiceB.hideStatus(false);
        } else if (strChoiceC.equals(correntAnswer)) {
            choiceC.setBackgroundResource(R.drawable.ic_item_correct);
            choiceC.setTextAnswerColor(android.R.color.white);
            choiceC.hideStatus(false);
        } else if (strChoiceD.equals(correntAnswer)) {
            choiceD.setBackgroundResource(R.drawable.ic_item_correct);
            choiceD.setTextAnswerColor(android.R.color.white);
            choiceD.hideStatus(false);
        }
    }
}

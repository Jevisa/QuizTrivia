package com.morfolo.quiztrivia.views.costum;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by Morfolo on 24/11/2017.
 */

public class LinearRectangleLayout extends LinearLayout {

    private static final double WIDTH_RATIO = 2.0;
    private static final double HEIGHT_RATIO = 2.0;

    public LinearRectangleLayout(Context context) {
        super(context);
    }

    public LinearRectangleLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LinearRectangleLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LinearRectangleLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = (int) ((HEIGHT_RATIO / WIDTH_RATIO) * widthSize);
        int newHeightSize = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, newHeightSize);
    }
}

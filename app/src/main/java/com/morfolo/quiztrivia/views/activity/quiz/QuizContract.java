package com.morfolo.quiztrivia.views.activity.quiz;

import com.morfolo.quiztrivia.internals.data.local.Quiz;
import com.morfolo.quiztrivia.views.base.IView;

import java.util.ArrayList;

/**
 * Created by Morfolo on 26/11/2017.
 */

public interface QuizContract {

    interface View extends IView {

        void onStartProgress();

        void onStopProgress();

        void onLoadSuccess(ArrayList<Quiz.Results> results);

        void onResetSuccess();

        void onError();
    }

    interface Presenter {

        void getQuestionFromServer(int category, String difficulty);

        void resetTokenFromServer();
    }
}

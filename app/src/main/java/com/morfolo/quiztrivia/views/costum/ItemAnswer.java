package com.morfolo.quiztrivia.views.costum;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.morfolo.quiztrivia.R;

/**
 * Created by Morfolo on 26/11/2017.
 */

public class ItemAnswer extends RelativeLayout {

    private static final double WIDTH_RATIO = 2.0;
    private static final double HEIGHT_RATIO = 2.0;

    private TextView textChoice;
    private TextView textAnswer;
    private TextView textStatus;

    private ItemAnswerListener mListener;

    public void setItemAnswerListener(ItemAnswerListener listener) {
        mListener = listener;
    }

    public interface ItemAnswerListener {
        void onClickItem(View view);
    }

    public ItemAnswer(Context context) {
        super(context);
        init(context);
    }

    public ItemAnswer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ItemAnswer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ItemAnswer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = (int) ((HEIGHT_RATIO / WIDTH_RATIO) * widthSize);
        int newHeightSize = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, newHeightSize);
    }

    private void init(Context context) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_answer, null);

        textChoice = view.findViewById(R.id.textChoice);
        textAnswer = view.findViewById(R.id.textAnswer);
        textStatus = view.findViewById(R.id.textStatus);

        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view.setClickable(true);
        view.setFocusable(true);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null){
                    mListener.onClickItem(view);
                }
            }
        });

        addView(view);
    }

    public void setTextAnswerColor(@ColorRes int color) {
        textAnswer.setTextColor(getResources().getColor(color));
    }

    public void hideStatus(boolean status) {
        if (status) {
            textStatus.setVisibility(GONE);
        } else {
            textStatus.setVisibility(VISIBLE);
        }
    }

    public void setTextChoice(String text) {
        textChoice.setText(text);
    }

    public void setTextAnswer(String text) {
        textAnswer.setText(text);
    }

    public void setTextStatus(String text) {
        textStatus.setText(text);
    }
}

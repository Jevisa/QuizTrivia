package com.morfolo.quiztrivia.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morfolo.quiztrivia.R;
import com.morfolo.quiztrivia.utils.IConstans;
import com.morfolo.quiztrivia.views.costum.LinearRectangleLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.ItemVH> {

    private Context context;

    private CategoryListener mListener;

    public interface CategoryListener {
        void onItemClick(String category, int code);
    }

    public CategoryRecyclerAdapter(Context context) {
        this.context = context;
        this.mListener = (CategoryListener) context;
    }

    @Override
    public ItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_category, parent, false);
        return new ItemVH(view);
    }

    @Override
    public void onBindViewHolder(final ItemVH holder, int position) {

        holder.container.setBackgroundResource(IConstans.Data.IMAGE[position]);

        Glide.with(context)
                .load(IConstans.Data.ICON[position])
                .into(holder.iconCategory);

        holder.textCategory.setText(IConstans.Data.TITLE[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(
                        context.getString(IConstans.Data.TITLE[holder.getAdapterPosition()]),
                        IConstans.Data.CATEGORY[holder.getAdapterPosition()]
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return IConstans.Data.CATEGORY.length;
    }

    public class ItemVH extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        LinearRectangleLayout container;
        @BindView(R.id.iconCategory)
        ImageView iconCategory;
        @BindView(R.id.textCategory)
        TextView textCategory;

        public ItemVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

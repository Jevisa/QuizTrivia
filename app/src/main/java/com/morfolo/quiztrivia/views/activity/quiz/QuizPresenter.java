package com.morfolo.quiztrivia.views.activity.quiz;

import com.morfolo.quiztrivia.internals.data.local.Quiz;
import com.morfolo.quiztrivia.internals.data.local.Reset;
import com.morfolo.quiztrivia.internals.data.remote.RestApi;
import com.morfolo.quiztrivia.utils.CacheManager;
import com.morfolo.quiztrivia.utils.IConstans;
import com.morfolo.quiztrivia.views.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Morfolo on 26/11/2017.
 */

public class QuizPresenter extends BasePresenter<QuizContract.View> implements QuizContract.Presenter {

    @Inject
    RestApi mApi;

    private static final int AMOUNT = 20;
    private static final String TYPE = "multiple";
    private static final String COMMAND = "reset";

    @Inject
    public QuizPresenter() {
    }

    @Override
    public void getQuestionFromServer(int category, String difficulty) {
        getView().onStartProgress();

        String token = CacheManager.getInstance().getString(IConstans.Pref.token);

        getDisposable().add(mApi.getQuiz(AMOUNT, category, difficulty.toLowerCase(), TYPE, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Quiz>() {
                    @Override
                    public void onNext(Quiz quiz) {
                        if(quiz.getResponseCode() == 0){
                            getView().onLoadSuccess(quiz.getResults());
                        }else {
                            getView().onError();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        getView().onStopProgress();
                        getView().onError();
                    }

                    @Override
                    public void onComplete() {
                        getView().onStopProgress();
                    }
                })
        );
    }

    @Override
    public void resetTokenFromServer() {
        getView().onStartProgress();

        String token = CacheManager.getInstance().getString(IConstans.Pref.token);

        getDisposable().add(mApi.getReset(COMMAND, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Reset>() {
                    @Override
                    public void onNext(Reset reset) {
                        if(reset.getResponseCode() == 0){
                            getView().onResetSuccess();
                        }else {
                            getView().onError();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        getView().onStopProgress();
                        getView().onError();
                    }

                    @Override
                    public void onComplete() {
                        getView().onStopProgress();
                    }
                })
        );
    }
}

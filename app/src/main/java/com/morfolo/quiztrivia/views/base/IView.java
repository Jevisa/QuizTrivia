package com.morfolo.quiztrivia.views.base;

/**
 * Created by Morfolo on 25/11/2017.
 */

public interface IView {

    void inject();

    void onAttachView();

    void onDetachView();
}

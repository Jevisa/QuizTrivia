package com.morfolo.quiztrivia.views.activity.score;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.morfolo.quiztrivia.R;
import com.morfolo.quiztrivia.utils.CacheManager;
import com.morfolo.quiztrivia.utils.IConstans;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Morfolo on 25/11/2017.
 */

public class ScoreActivity extends AppCompatActivity {

    private Unbinder mUnbinder;

    @BindView(R.id.textEasy)
    TextView textEasy;
    @BindView(R.id.textMedium)
    TextView textMedium;
    @BindView(R.id.textHard)
    TextView textHard;

    public static void startThisActivity(Context context) {
        Intent intent = new Intent(context, ScoreActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        mUnbinder = ButterKnife.bind(this);

        textEasy.setText(String.valueOf(CacheManager.getInstance().getInt(IConstans.Pref.easy)));
        textMedium.setText(String.valueOf(CacheManager.getInstance().getInt(IConstans.Pref.medium)));
        textHard.setText(String.valueOf(CacheManager.getInstance().getInt(IConstans.Pref.hard)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }
}

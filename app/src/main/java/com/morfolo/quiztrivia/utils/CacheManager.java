package com.morfolo.quiztrivia.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.morfolo.quiztrivia.BaseApp;

/**
 * Created by Morfolo on 24/11/2017.
 */

public class CacheManager {

    private static CacheManager sInstance;

    public static synchronized CacheManager getInstance() {
        if (sInstance == null) {
            sInstance = new CacheManager();
        }
        return sInstance;
    }

    private SharedPreferences getPref() {
        Context context = BaseApp.getComponent().getContext();
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public int getInt(String key) {
        return getPref().getInt(key, 0);
    }

    public void putInt(String key, int value) {
        getPref().edit().putInt(key, value).apply();
    }

    public String getString(String key) {
        return getPref().getString(key, "");
    }

    public void putString(String key, String value) {
        getPref().edit().putString(key, value).apply();
    }
}

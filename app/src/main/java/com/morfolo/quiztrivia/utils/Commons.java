package com.morfolo.quiztrivia.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;

import com.morfolo.quiztrivia.BaseApp;

/**
 * Created by Morfolo on 24/11/2017.
 */

public class Commons {

    public static String toText(String html) {
        String text;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            text = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            text = Html.fromHtml(html).toString();
        }
        return text;
    }
}

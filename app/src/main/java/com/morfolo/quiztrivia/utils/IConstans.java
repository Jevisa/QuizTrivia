package com.morfolo.quiztrivia.utils;

import com.morfolo.quiztrivia.R;

/**
 * Created by Morfolo on 24/11/2017.
 */

public interface IConstans {

    interface Url {
        String baseUrl = "https://opentdb.com/";
    }

    interface Data {
        int[] TITLE = {
                R.string.label_general_knowledge, R.string.label_entertainment_books, R.string.label_entertainment_film,
                R.string.label_entertainment_music, R.string.label_entertainment_games, R.string.label_entertainment_tv,
                R.string.label_science_computers, R.string.label_celebrities, R.string.label_history, R.string.label_animals
        };

        int[] ICON = {
                R.drawable.ic_knowledge, R.drawable.ic_book, R.drawable.ic_film, R.drawable.ic_music, R.drawable.ic_games,
                R.drawable.ic_tv, R.drawable.ic_computer, R.drawable.ic_celeb, R.drawable.ic_history, R.drawable.ic_animal
        };

        int[] IMAGE = {
                R.drawable.img_knowledge, R.drawable.img_book, R.drawable.img_film, R.drawable.img_music, R.drawable.img_fames,
                R.drawable.img_tv, R.drawable.img_computer, R.drawable.img_celeb, R.drawable.img_history, R.drawable.img_animal
        };

        int[] CATEGORY = {9, 10, 11, 12, 15, 14, 18, 26, 23, 27};
    }

    interface Pref {
        String token = "token";

        String easy = "easy";
        String medium = "medium";
        String hard = "hard";
    }

    interface Extra {
        String code = "code";
        String difficulty = "difficulty";
        String category = "category";
        String item = "item";
    }

    interface Argument {
        String position = "position";
        String item = "item";
    }
}

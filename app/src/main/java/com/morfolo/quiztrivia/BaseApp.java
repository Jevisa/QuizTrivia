package com.morfolo.quiztrivia;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.morfolo.quiztrivia.internals.injector.component.ApplicationComponent;
import com.morfolo.quiztrivia.internals.injector.component.DaggerApplicationComponent;
import com.morfolo.quiztrivia.internals.injector.module.ApplicationModule;
import com.morfolo.quiztrivia.internals.injector.module.NetworkModule;

/**
 * Created by Morfolo on 24/11/2017.
 */

public class BaseApp extends Application {

    private static volatile ApplicationComponent mComponent;

    public static synchronized ApplicationComponent getComponent() {
        return mComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .build();
        mComponent.inject(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
